var resolve = require('rollup-plugin-node-resolve');
var buble = require('rollup-plugin-buble');
var cjs = require('rollup-plugin-commonjs');

const config = {
  output: {
    indent: false,
    extend: true,
  },
  plugins: [
    buble(),
    resolve(
     {
        customResolveOptions: {
            moduleDirectory: ['/usr/share/nodejs', '/usr/lib/nodejs'],
            preferBuiltins: false
          }
     }
    ),
    cjs(),
  ]
};

export default [
  config
];
